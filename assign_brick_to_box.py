import numpy as np
from cameras import lower_camera
from embeddings import model, get_lego_crop, get_closest_box_vae

def find_cluster(clusters_latent_vecs, rgb_frame, segmented_frame, closest_object):
	"""
	Find the nearest cluster of a brick at the bottom of the frame
	and return its marker_id.
	@param clusters_latent_vecs: dictionary of latent mu, std and
	logvar vectors for each box.
	@param rgb_frame: the rgb image to crop the lego from
	@param segmented_frame: the corresponding segmented image
	@param closest_object: tuple of the lego position (x,y,w,h)
	"""
	
	# lego_crop is 64x64 pixels
	lego_crop = get_lego_crop(rgb_frame, segmented_frame, closest_object)
	latent_mu, latent_std, latent_logvar = model.get_latent_vectors(lego_crop)
	
	# find nearest neighbor with KL
	marker_id = get_closest_box_vae(clusters_latent_vecs, (latent_mu, latent_std, latent_logvar))
	print("==========================================================")
	print("Going to box %s" % str(marker_id))
	print("==========================================================")
	return marker_id

