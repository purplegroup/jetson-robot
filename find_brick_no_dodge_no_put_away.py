import time
import random
import cv2
import numpy as np

from robot import r as robot
from cameras import lower_camera
from bound_bricks import bound_bricks
from segmentation import get_frame, cleanup
from find_box import navigate_to_box
from assign_brick_to_box import find_cluster


middle_actions = [lambda: robot.turn(45, 90, wait=False), 
				  lambda: robot.turn(-45, 90, wait=False)]
edge_actions = [lambda: robot.turn(90, 90, wait=True), 
				lambda: robot.turn(-90, 90, wait=True)]
exploration_actions = [lambda: robot.turn(360, 45, return_time=True),
						lambda: robot.move(12, 90, return_time=True)]


def middle_bricks(vertical_position):
	"""
	Determine the horizontal position of the bricks middle lines for 
	the given vertical_position. 
	"""
	center = int(width/2)
	bottom_deviation = int(width/6)
	top_deviation = int(width/3)
	deviation = lambda y: int(top_deviation + (bottom_deviation - top_deviation) * (y / height))
	return center - deviation(vertical_position), center + deviation(vertical_position)			


def navigate_to_brick(clusters_latent_vecs):
	"""
	Search for the nearest brick while assuming there are no obstacles 
	in the workspace.
	"""
	global width
	global height
	i = 0
	i_time = time.time()
	wait_time = 0
	while True:
		rgb_frame, segmented_frame = get_frame(lower_camera)
		t0 = time.time()
		
		# get frame with contours
		rgb_frame, brick_rectangles, edge_in_frame, middle_bottom_y = bound_bricks(segmented_frame, rgb_frame)
		print("Detection time {}".format(time.time() - t0))
		height, width = rgb_frame.shape[:2]
		
		# draw bricks middle lines
		cv2.line(rgb_frame, (middle_bricks(0)[0], 0), (middle_bricks(height-1)[0], height-1), (0, 255, 0))
		cv2.line(rgb_frame, (middle_bricks(0)[1], 0), (middle_bricks(height-1)[1], height-1), (0, 255, 0))
		
		cv2.imshow("lower rgb frame", rgb_frame)
		cv2.waitKey(10)
		
		# find the closest brick
		max_height = 0
		closest_brick = (0, 0, 0, 0)
		for rectangle in brick_rectangles:
			if (rectangle[1] + rectangle[3]) >= max_height:
				max_height = rectangle[1] + rectangle[3]
				closest_brick = rectangle
				
		brick_center = (int(closest_brick[0]+closest_brick[2]/2), int(closest_brick[1]+closest_brick[3]-1))
				
		# edges in frame
		if edge_in_frame:
			if ((not brick_rectangles) and (middle_bottom_y > height/3)) or (brick_rectangles and middle_bottom_y > brick_center[1]):
				print("CLOSE EDGE!!!")
				#random.choice(edge_actions)()
				robot.turn_forever(-45)
				continue

		# no bricks in frame
		if not brick_rectangles:
			print("EXPLORING...")
			if (time.time() - i_time) > wait_time:
				wait_time = exploration_actions[i % len(exploration_actions)]()
				i = i + 1
				i_time = time.time()
		
		# bricks in frame, navigate to the closest one	
		else:
			i = 0
			i_time = time.time()
			wait_time = 0
		
			

			print("brick")
			# brick is on the left	
			if brick_center[0] < middle_bricks(brick_center[1])[0]:
				robot.turn_forever(45)
				print("TURNING LEFT, {}".format(closest_brick))
			# brick is on the right
			elif brick_center[0] > middle_bricks(brick_center[1])[1]:
				robot.turn_forever(-45)
				print("TURNING RIGHT, {}".format(closest_brick))
			# brick is near
			elif (closest_brick[1] + closest_brick[3]) > (height - 30):
				print("PICK, {}".format(closest_brick))
				robot.freeze()
				# decide which box to go to according to the brick cluster 
				marker_id = find_cluster(clusters_latent_vecs, rgb_frame, segmented_frame, closest_brick)
				robot.pick_manouver()
				navigate_to_box(marker_id, bricks_only=True)
				#break
			# brick is straight
			else:
				robot.move_forever(90)
				print("MOVING FORWARD, {}".format(closest_brick))


if __name__ == "__main__":
	navigate_to_brick()
	# release the capture
	lower_camera.release()
	cv2.destroyAllWindows()
