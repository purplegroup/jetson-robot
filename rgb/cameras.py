UPPER_CAMERA = 1
LOWER_CAMERA = 2

import cv2
import time
from threading import Thread, Lock

def grab_frames(camera, lock):
    while not lock.locked():
        camera.grab()
        time.sleep(0.01)
    print("Finished grabbing")

upper_camera=cv2.VideoCapture(UPPER_CAMERA)
upper_lock = Lock()
        
upper_t = Thread(target=grab_frames, args=(upper_camera, upper_lock))
upper_t.start()

lower_camera=cv2.VideoCapture(LOWER_CAMERA)
lower_lock = Lock()
        
lower_t = Thread(target=grab_frames, args=(lower_camera, lower_lock))
lower_t.start()

time.sleep(2)
