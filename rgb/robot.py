import rpyc
import time
import math

conn = rpyc.classic.connect('ev3dev.local') # host name or IP address of the EV3
ev3 = conn.modules['ev3dev.ev3']      # import ev3dev.ev3 remotely

class Robot(object):
	def __init__(self):
		self.right_motor = ev3.LargeMotor('outC')
		self.left_motor = ev3.LargeMotor('outA')
		self.claw = ev3.MediumMotor('outB')
		self.motors = [self.right_motor, self.left_motor, self.claw]
#        ev3.Sound.speak('Yaaaaawn!')
		for m in self.motors:
			m.reset()
			m.run_timed(time_sp=100, speed_sp=360)
			m.run_timed(time_sp=100, speed_sp=-360)
			time.sleep(0.1)
			m.stop_action = 'brake'
			m.stop()
		self.claw.run_timed(time_sp=1000, speed_sp=-500)
		time.sleep(1)
		print("Ready to go!")

		self.duration2distance = 53.8/5000. # at 360 ticks/milisecond
		self.ticks2distance = (self.duration2distance * 1000) / 360 # centimeters/tick
		self.duration2degree = 90./1300.
		self.ticks2rotation = (self.duration2degree * 1000) / 360
		#self.sleep_time=0;

	def move(self,distance=20,speed=360, wait=False, return_time=False):
		#print(self.sleep_time)
		ticks = distance / self.ticks2distance
		self.right_motor.run_to_rel_pos(position_sp=ticks, speed_sp=speed)
		self.left_motor.run_to_rel_pos(position_sp=ticks, speed_sp=speed)
		if wait:
			time.sleep(abs(ticks/speed))
		elif return_time:
			return abs(ticks/speed)
		#self.sleep_time=self.sleep_time+duration/1000;

	def turn(self,angle=90,speed=180, wait=False, return_time=False):
		ticks = angle / self.ticks2rotation
		self.left_motor.run_to_rel_pos(position_sp=-ticks, speed_sp=speed)
		self.right_motor.run_to_rel_pos(position_sp=ticks, speed_sp=speed)
		if wait:
			time.sleep(abs(ticks/speed))
		elif return_time:
			return abs(ticks/speed)

	def move_to(self, distance_x, distance_y):
		r.turn(math.atan2(distance_y, distance_x)*180/math.pi, wait=True, speed=45)
		r.move(math.sqrt(distance_x**2 + distance_y**2) - 10, wait=True, speed=90)
		r.pick()

	def move_forever(self, speed=180):
		#print(self.sleep_time)
		self.right_motor.run_forever(speed_sp=speed)
		self.left_motor.run_forever(speed_sp=speed)
#        time.sleep(abs(ticks/speed))
		#self.sleep_time=self.sleep_time+duration/1000;

	def turn_forever(self, speed=90):
		self.left_motor.run_forever(speed_sp=-speed)
		self.right_motor.run_forever(speed_sp=speed)
#        time.sleep(abs(angle/1000.))

	def pick(self,claw_speed=360):
		self.claw.run_to_rel_pos(position_sp=720, speed_sp=claw_speed)

	def drop(self,claw_speed=360):
		self.claw.run_to_rel_pos(position_sp=-720, speed_sp=claw_speed)

	def pick_manouver(self,speed=90):
		self.move(distance=5, speed=speed, wait=True)
		self.pick()
		time.sleep(3)

	def drop_manouver(self,speed=90):
		self.drop()
		time.sleep(1)
		self.move(distance=-10, speed=speed)

	def drop_manouver_box(self, speed=90):
		self.move(distance=18, speed=speed, wait=True)  #TODO: maybe we need to tune
		self.drop()
		time.sleep(1)
		self.move(distance=-15, speed=speed, wait=True)
		self.turn(angle=180, speed=45, wait=True)

	def out_of_the_way_manouver(self,speed=90):
		self.move(distance=3, speed=speed, wait=True)
		self.pick()
		time.sleep(1)
		self.turn(angle=90, wait=True)
		self.move(distance=6, speed=speed, wait=True)
		self.drop()
		time.sleep(1)
		self.move(distance=-6, speed=speed, wait=True)
		self.turn(angle=-90, wait=True)
		self.move(distance=-3, speed=speed, wait=True)

	def freeze(self):
		for m in self.motors:
			m.stop_action = 'hold'
			m.stop()
			m.stop_action = 'coast'

r = Robot()

# t0 = time.time()
# while time.time() - t0 < 2:
#     r.move(speed=120)
# print('done')
# r.freeze()
