import argparse
import find_brick_no_dodge_no_put_away
import find_brick_and_put_away 
import find_brick_and_dodge
import assign_rgb_vecs_to_boxes


MAX_NUM_OF_BOXES = 6


def play_demo(num_of_boxes, behaviour):
	"""
	Main script to sort bricks using the RGB method.
	"""
	assert num_of_boxes <= MAX_NUM_OF_BOXES
	
	# get clusters rgb vectors dictionary 
	clusters_rgb_vecs = assign_rgb_vecs_to_boxes.assign(num_of_boxes)
	input("press ENTER to start sorting")
	
	# select demo behaviour
	if behaviour == "simple":
		find_brick_no_dodge_no_put_away.navigate_to_brick(clusters_rgb_vecs)
	elif behaviour == "put away":
		find_brick_and_put_away.navigate_to_brick(clusters_rgb_vecs)
	elif behaviour == "dodge": 
		find_brick_and_dodge.navigate_to_brick(clusters_rgb_vecs)
	else:
		print("unrecognized behaviour.")
		print("select 'simple' when no obstacles are in the workspace.")
		print("select 'put away' to put away obstacles (only if small and light obstacles are in the worksapce!).")
		print("select 'dodge' to dodge obstacles.")


parser = argparse.ArgumentParser()
parser.add_argument('--behaviour', type=str, help='simple, put away, dodge')
parser.add_argument('--boxes', type=int, help='number of boxes in the workspace')
args = parser.parse_args()
behaviour = args.behaviour
num_of_boxes = args.boxes
play_demo(num_of_boxes, behaviour)


