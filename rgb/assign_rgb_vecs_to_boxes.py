import numpy as np
from cameras import lower_camera
from segmentation import get_frame, cleanup
from get_lego_rgb import get_lego_rgb


NUM_OF_IMAGES = 2
	

def compute_rgb_vecs():
	"""
	Compute the rgb vectors of a brick.
	Take NUM_OF_IMAGES images of different orientations, and compute
	their corresponding rgb vectors.
	"""
	rgb_vecs = []
	for i in range(NUM_OF_IMAGES):
		print("image %s" % str(i+1))
		input("press ENTER to capture image")
		rgb_frame, segmented_frame = get_frame(lower_camera)
		rgb_vec = get_lego_rgb(rgb_frame, segmented_frame)
		rgb_vecs.append(rgb_vec)
	return rgb_vecs
		

def assign(num_of_boxes):
	"""
	Assign the clusters' rgb vectors to the boxes' id markers.
	1st brick is assigned to id=0, 2nd brick to id=1 and so on...
	"""
	
	# get the rgb vectors of each brick
	clusters_rgb_vecs = {}
	for i in range(num_of_boxes):
		print("Taking %s images for brick %s" % (str(NUM_OF_IMAGES), str(i+1)))
		rgb_vecs = compute_rgb_vecs()
		clusters_rgb_vecs[i] = rgb_vecs
	
	return clusters_rgb_vecs

