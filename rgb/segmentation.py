# coding: utf-8

print("Loading modules...")

import time
from threading import Thread, Lock

import cv2
import numpy as np
import tensorflow as tf
import math

import pydensecrf.densecrf as dcrf

from pydensecrf.utils import compute_unary, create_pairwise_bilateral,     create_pairwise_gaussian, unary_from_softmax

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

g_1 = tf.Graph()
sess1=tf.Session(graph=g_1, config=config)

#im_w, im_h = 480, 480
im_w, im_h = 240, 240
camera_mask = np.zeros((im_h, im_w))
camera_mask[160:,0:40]=255 # hardcoded to 240
camera_mask[160:,200:]=255 # hardcoded to 240
#camera_mask[0:20,:] = 255

print("Building graph...")

with g_1.as_default():
    X = tf.placeholder(tf.float32, [None,240,240,3])
    def model(X):
        Wconv1 = tf.get_variable('Wconv1',shape=[3,3,3,128],initializer=tf.contrib.layers.xavier_initializer())
        B1=tf.get_variable('Bconv1',shape=[128])
        Wconv2 = tf.get_variable('Wconv2',shape=[3,3,128,64],initializer=tf.contrib.layers.xavier_initializer())
        B2 = tf.get_variable('Bconv2',shape=[64])
        Wconv3 = tf.get_variable('Wconv3',shape=[3,3,64,3],initializer=tf.contrib.layers.xavier_initializer())
        B3 = tf.get_variable('Bconv3',shape=[3])
        
        a1=tf.nn.atrous_conv2d(X,Wconv1,2,padding='SAME')+B1
        h1=tf.nn.relu(a1)
        
        a2=tf.nn.atrous_conv2d(h1,Wconv2,2,padding='SAME')+B2 #strides=[1,1,1,1]
        h2=tf.nn.relu(a2)
        
        a3=tf.nn.atrous_conv2d(h2,Wconv3,2,padding='SAME')+B3 #strides=[1,1,1,1]
        h3=tf.nn.relu(a3)
        yout=tf.reshape(h3,[-1,3])###########yout.shape=[bach*480*480,3]
        return yout
    y_out=model(X)    
    saver1 = tf.train.Saver()
    print("Restoring checkpoint...")
    saver1.restore(sess1, "checkpoint/checkpoints.ckpt")
    

def run_model1(session,predict,Xd,show_segmentation):
        
    variables=[predict]
    ################Session###################################
    feed_dict={X:Xd}
    y_output=session.run(variables,feed_dict=feed_dict)
    ################Session###################################
    
    ####################CFD###################################              
    y_pridict=np.argmax(y_output,axis=-1)
    y_pridict=y_pridict.reshape((240,240))
    temp2=y_pridict.astype('int8')
    tempcv2 = (temp2/2.0*255).astype('uint8')
    if show_segmentation:
        cv2.imshow('without classifier',tempcv2)
        cv2.waitKey(33)
    return tempcv2


def get_frame(camera, get_segmentation=True, apply_segm_mask=True, show_segmentation=True):

    t0 = time.time()
    return_value, image = camera.retrieve()

    image = cv2.resize(image,(im_w,im_h))

    image = np.expand_dims(image, axis=0)
    original = image.copy()
    image = (image/255.-0.5)*2
    t1 = time.time()
    #print("Input time {}".format(t1 - t0))

    masked_segm_image = None
    if get_segmentation:
        segm_image = run_model1(sess1, y_out, image,
                               show_segmentation=show_segmentation)
        t2 = time.time()
        #print("Segm time {}".format(t2 - t1))

        cv2.imshow("mask", camera_mask)
        if apply_segm_mask:
            masked_segm_image = np.clip(segm_image + camera_mask, 0,255)
        else:
            masked_segm_image = segm_image
        
    return original.squeeze(), masked_segm_image

def cleanup():
    camera.release()
    lock.acquire(timeout=1)
    t.join(1)
    sess.close()
    
print("Done loading.")

if __name__ == "__main__":
	from cameras import lower_camera
	while True:
		img, segm_img = get_frame(lower_camera)
		cv2.imshow("rgb img", img)
		cv2.waitKey(10)
		

