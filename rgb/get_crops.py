from threading import Thread, Lock
import time

lock = Lock()
 
def grab_frames(camera, lock):
    while not lock.locked():
        camera.grab()
        time.sleep(0.01)
    print("Finished grabbing")
    
t = Thread(target=grab_frames, args=(camera, lock))
t.start()



def get_lego_crops(img, plabel, frame=1):
    new_plabel = np.zeros_like(plabel)
    new_plabel[np.where(plabel ==1)] = 1
    labels = measure.label(new_plabel, neighbors=8, background=0)
    lego_crops = dict()
    template_size = 128
    #pdb.set_trace()
    
    
    for label in np.unique(labels):
        if label == 0:
            continue
            
        labelMask = np.zeros(new_plabel.shape, dtype="uint8")
        labelMask[labels == label] = 1
        numPixels = cv2.countNonZero(labelMask)
        indices = np.where(labelMask == 1)
        #pdb.set_trace()
        
        if numPixels >50 and numPixels<50000:
            #pdb.set_trace()
            block_only = np.zeros_like(img)
            block_only[indices] = img[indices]
            
            contours = cv2.findContours(labelMask.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            
            
            contour_list = list()
            
            
            for conts in contours[1]:
                contour_list.append(np.squeeze(conts))
            
            
            contours = np.vstack(contour_list)
            left_min , top_min  = contours.min(axis = 0)
            left_max , top_max  = contours.max(axis = 0)
            if (top_max -top_min) <1 or (left_max - left_min)<1 or ((top_max -top_min) * (left_max - left_min) <0):
                continue
            
            cropped = block_only[top_min:top_max, left_min:left_max]#[...,::-1]
            resize_shape = tuple((np.array(cropped.shape)/max(cropped.shape)*template_size).astype("uint8"))
            resized_crop = cv2.resize(cropped.copy(), (resize_shape[1], resize_shape[0])) 
            template = np.zeros((template_size,template_size,3))
            
            #plt.imshow(resized_crop)
            #plt.show()
            
            template[int((template_size - resize_shape[0])/2):int((template_size - resize_shape[0])/2)+resize_shape[0], int((template_size - resize_shape[1])/2):resize_shape[1]+int((template_size - resize_shape[1])/2),:] = resized_crop.copy()
            lego_crops[label] = template.astype("uint8")
            #cv2.imwrite("whatever/"+ str(frame)+"_"+str(label)+".png",template.astype("uint8"))
            
    return lego_crops
            
while(True):
    return_value, image =camera.retrieve()
    cv2.imshow("asd", image)
    #get_lego_crops()
    cv2.waitKey(0)
