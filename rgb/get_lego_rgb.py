import numpy as np


def get_lego_rgb(img, plabel, bbox=(0,120,240,120), frame=1):
    bbox_mask = np.zeros_like(plabel)
    bbox_mask[bbox[1]:np.clip((bbox[1]+bbox[3]), 0 , 239) ,bbox[0]:np.clip((bbox[0]+bbox[2]),0, 239)] = 1
    plabel = plabel * bbox_mask
    
    new_plabel = np.zeros_like(plabel)
    new_plabel[np.where(plabel ==127)] = 1
    
    return np.mean(img[np.where(plabel ==127)], axis = 0)
    
    
def get_closest_box(box_to_embedding, test_vector):
    for box in box_to_embedding:
        box_to_embedding[box] = np.vstack(box_to_embedding[box])

    min_index = None
    min_distance = 100000000000
    for box in box_to_embedding.keys():
        distances = np.linalg.norm(box_to_embedding[box] - test_vector)
        if np.min(distances)< min_distance:
            min_distance = np.min(distances)
            min_index = box

    return min_index
