import cv2
import numpy as np


bgr_colors = {"brick" : (255, 0, 0),
	      	  "obstacle": (0, 0, 255),
	      	  "edge": (0, 255, 255)}

class_colors = {"obstacle": 0, "brick": 127, "background": 255}


def bound_objects(segmented_frame, rgb_frame, is_lower_camera=True):
	"""
	Draw bounding forms around bricks, obstacles and edges in frame.
	"""
	height, width = rgb_frame.shape[:2]
	brick_frame = np.zeros((height, width, 1), np.uint8)
	brick_frame[np.where(segmented_frame == class_colors["brick"])] = 255
	obstacle_frame = np.zeros((height, width, 1), np.uint8)
	obstacle_frame[np.where(segmented_frame == class_colors["obstacle"])] = 255
	if is_lower_camera:
		# lower camera: ignore upper 1/8
		obstacle_frame[:obstacle_frame.shape[1]//8,:,:] = 0
	else:
		# upper camera: ignore upper 1/4
		obstacle_frame[:obstacle_frame.shape[1]//4,:,:] = 0
		
	# correct segmentation 
	kernel = np.ones((10,10),np.uint8)
	dilation = cv2.dilate(brick_frame, kernel, iterations = 1)
	erosion = cv2.erode(obstacle_frame, kernel, iterations = 1)
	closing = cv2.morphologyEx(erosion, cv2.MORPH_CLOSE, kernel)
	new_segmented_frame = np.full((height, width, 1), 255, np.uint8)
	new_segmented_frame[dilation == 255] = 127
	if is_lower_camera:
		new_segmented_frame[closing == 255] = 0
	else:
		new_segmented_frame[obstacle_frame == 255] = 0
	cv2.imshow("new segmented frame", new_segmented_frame)
	
	# bricks bounding rectangles
	_, contours, hierarchy = cv2.findContours(dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	object_rectangles = []
	for cnt in contours:
		(x,y,w,h) = cv2.boundingRect(cnt)
		object_rectangles += [(x,y,w,h)]
		cv2.rectangle(rgb_frame, (x,y), (x+w,y+h), bgr_colors["brick"], 2)
		# mark the middle point of the rectangle's lower edge
		rgb_frame[int(y+h-1), int(x+w/2)] = bgr_colors["brick"]

	# obstacles bounding rectangles, edges bounding contours
	_, contours, hierarchy = cv2.findContours(erosion, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	edge_in_frame = False
	middle_bottom_y = None
	for i, cnt in enumerate(contours):
		# ignore inner contours
		if hierarchy[0, i, 3] != -1:
			continue
		(x,y,w,h) = cv2.boundingRect(cnt)
		# ignore noise
		if w*h < 500:
			continue
		# edge
		if w > (width * 3/4):
			edge_in_frame = True
			cv2.drawContours(rgb_frame, contours, i, bgr_colors["edge"], 2)
			middle_bottom_y = max([j for j in range(height) if np.all(rgb_frame[j,int(width/2)] == np.asarray(bgr_colors["edge"]))])
			middle_bottom_point = (int(width/2), middle_bottom_y)
			cv2.circle(rgb_frame, middle_bottom_point, 8, (128, 128, 0), -1)
		# obstacle
		else:
			object_rectangles += [(x,y,w,h)]
			cv2.rectangle(rgb_frame, (x,y), (x+w,y+h), bgr_colors["obstacle"], 2)
		
	return rgb_frame, object_rectangles, edge_in_frame, middle_bottom_y

