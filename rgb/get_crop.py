import cv2
import pdb
import numpy as np
from skimage import measure
import matplotlib.pyplot as plt



def get_lego_crop(img, plabel, bbox=(0,120,240,120), frame=1):
    #plabel = (1.0*plabel/255.0 *2).astype("int")
    #pdb.set_trace()
    bbox_mask = np.zeros_like(plabel)
    bbox_mask[bbox[1]:np.clip((bbox[1]+bbox[3]), 0 , 239) ,bbox[0]:np.clip((bbox[0]+bbox[2]),0, 239)] = 1
    plabel = plabel * bbox_mask
    
    new_plabel = np.zeros_like(plabel)
    new_plabel[np.where(plabel ==127)] = 1

    #pdb.set_trace()
    labels = measure.label(new_plabel, neighbors=8, background=0)
    lego_crops = dict()
    template_size = 64
    #pdb.set_trace()
    
    
    for label in np.unique(labels):
        if label == np.min(labels):
            continue
            
        labelMask = np.zeros(new_plabel.shape, dtype="uint8")
        labelMask[labels == label] = 1
        numPixels = cv2.countNonZero(labelMask)
        indices = np.where(labelMask == 1)
        #pdb.set_trace()
        
        if numPixels >50 and numPixels<50000:
            #pdb.set_trace()
            block_only = np.zeros_like(img)
            block_only[indices] = img[indices]
            
            contours = cv2.findContours(labelMask.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
            
            
            contour_list = list()
            
            
            for conts in contours[1]:
                contour_list.append(np.squeeze(conts))
            
            
            contours = np.vstack(contour_list)
            left_min , top_min  = contours.min(axis = 0)
            left_max , top_max  = contours.max(axis = 0)
            if (top_max -top_min) <1 or (left_max - left_min)<1 or ((top_max -top_min) * (left_max - left_min) <0):
                continue
            
            cropped = block_only[top_min:top_max, left_min:left_max]#[...,::-1]
            resize_shape = tuple((np.array(cropped.shape)/max(cropped.shape)*template_size).astype("uint8"))
            resized_crop = cv2.resize(cropped.copy(), (resize_shape[1], resize_shape[0])) 
            template = np.zeros((template_size,template_size,3))
            
            #plt.imshow(resized_crop)
            #plt.show()
            
            template[int((template_size - resize_shape[0])/2):int((template_size - resize_shape[0])/2)+resize_shape[0], int((template_size - resize_shape[1])/2):resize_shape[1]+int((template_size - resize_shape[1])/2),:] = resized_crop.copy()
            return template.astype("uint8")
            #lego_crops[label] = template.astype("uint8")
            #cv2.imwrite("whatever/"+ str(frame)+"_"+str(label)+".png",template.astype("uint8")) 
