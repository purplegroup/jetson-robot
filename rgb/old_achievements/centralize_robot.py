import cv2
import robot
from brick_detection_ import detect_brick, camera_lock
import time
import numpy as np
import random


green = np.asarray((0, 255, 0))
robot = robot.r
#actions = [lambda: robot.move_forever(90), lambda: robot.move_forever(-90), lambda: robot.turn_forever(45), lambda: robot.turn_forever(-45)]
actions = [lambda: robot.move(20, wait=True), lambda: robot.turn(45, wait=True), lambda: robot.turn(-45, wait=True)]

middle = (220, 420)
while True:
	rgb_frame, circles = detect_brick()
	cv2.line(rgb_frame, (middle[0], 0), (middle[0], 479), (0, 255, 0))
	cv2.line(rgb_frame, (middle[1], 0), (middle[1], 479), (0, 255, 0))
	cv2.imshow("{}".format(rgb_frame.shape), rgb_frame)
	cv2.waitKey(10)
	max_height = 0
	closest_circle = (0, 0, 0)

	for circle in circles:
		if circle[1] >= max_height:
			max_height = circle[1]
			closest_circle = circle

	if not circles:
		random.choice(actions)()	
	if closest_circle[0] < middle[0]:
		robot.turn_forever(45)
		print("Turning left, {}".format(closest_circle))
	elif closest_circle[0] > middle[1]:
		robot.turn_forever(-45)
		print("Turning right, {}".format(closest_circle))
	elif (closest_circle[1] + closest_circle[2]) > 480:
		if np.all(rgb_frame[closest_circle[1], closest_circle[0]] == green):
			robot.out_of_the_way_manouver()
		else:
			robot.freeze()
			robot.pick_manouver()
			break
	else:
		robot.move_forever(90)
		print("Moving forward, {}".format(closest_circle))

camera_lock.acquire() # stops frame grabbing

	

	
	



