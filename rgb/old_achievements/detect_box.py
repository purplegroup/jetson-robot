from __future__ import print_function
import cv2
import numpy as np
import time
import random
from threading import Thread, Lock
from robot import r as robot
from segmentation import get_frame, im_w, im_h
from detect_bricks2 import detect_bricks
import cv2.aruco as aruco
import pdb


from cameras import upper_camera

# from achievement_5_6
actions = [lambda: robot.turn(45, 45, wait=False), lambda: robot.turn(-45, 45, wait=False)]
actions2 = [lambda: robot.turn(45, 45, wait=True), lambda: robot.turn(-45, 45, wait=True)]
exploration_actions = [lambda: robot.move(20, 90, return_time=True),
						lambda: robot.turn(-180, 45, return_time=True),
						lambda: robot.turn(360, 45, return_time=True),
						lambda: robot.turn(-180, 45, return_time=True)]
def middle_obstacles(vertical_position):
	center = int(im_w/2)
	bottom_deviation = int(im_w/2)
	top_deviation = int(im_w/6)
	deviation = lambda y: int(top_deviation + (bottom_deviation - top_deviation) * (y / im_h))
	return center - deviation(vertical_position), center + deviation(vertical_position)
##	

frame_captured = False
while not frame_captured:
	frame_captured, frame = upper_camera.retrieve()

height_full_frame, width_full_frame = frame.shape[:2]
middle = (int(width_full_frame/3), int(2*width_full_frame/3))

def box_mode():
	height, width = im_h, im_w

	i = 0
	i_time = time.time()
	wait_time = 0
	while True:
		frame_captured, frame = upper_camera.retrieve()
		rgb_frame, segmented_frame = get_frame(upper_camera, apply_segm_mask=False)
		assert frame_captured
		
		rgb_frame, object_rectangles, edge_rectangles = detect_bricks(segmented_frame, rgb_frame, is_lower_camera=False)		
		cv2.line(rgb_frame, (middle_obstacles(0)[0], 0), (middle_obstacles(height -1)[0], height-1), (255, 255, 0))
		cv2.line(rgb_frame, (middle_obstacles(0)[1], 0), (middle_obstacles(height -1)[1], height-1), (255, 255, 0))
		
		# find the closest object
		max_height = 0
		closest_object = (0, 0, 0, 0)
		for rectangle in object_rectangles:
			if (rectangle[1] + rectangle[3]) >= max_height:
				max_height = rectangle[1] + rectangle[3]
				closest_object = rectangle
				
		closest_object_is_inside_edge_rectangle = False
		for edge_rectangle in edge_rectangles:
			if (edge_rectangle[1] + edge_rectangle[3]) > max_height:
				closest_object_is_inside_edge_rectangle = True
				break

		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
		parameters =  aruco.DetectorParameters_create()
	 
		#lists of ids and the corners beloning to each id
		corners, marker_ids, rejectedImgPoints = aruco.detectMarkers(frame, aruco_dict, parameters=parameters)
		try:
			if marker_ids.shape[0] == 1:
				marker_ids[0].tolist()
			else:
				marker_ids = np.squeeze(marker_ids).tolist()
			gray = aruco.drawDetectedMarkers(frame, corners, marker_ids, [0,255,0])
		except:
			marker_ids = []
	 
		cv2.line(frame, (middle[0], 0), (middle[0], height_full_frame-1), (0, 255, 0))
		cv2.line(frame, (middle[1], 0), (middle[1], height_full_frame-1), (0, 255, 0))
		
		cv2.imshow("rgb_frame2", rgb_frame)
		cv2.imshow('Test Frame', frame)
		cv2.waitKey(10)
		
		if edge_rectangles:
			edge_rectangles = np.asarray(edge_rectangles)
			if not 0 in marker_ids:
				print("CLOSE EDGE!!! {}".format(edge_rectangles))
				#random.choice(actions2)()
				robot.turn_forever(-45)
				continue
			
		if (not 0 in marker_ids) and (not object_rectangles):
			# no markers and objects in frame
			if (time.time() - i_time) > wait_time:
				wait_time = exploration_actions[i % 4]()
				i = i + 1
				i_time = time.time()
				
		elif object_rectangles and not closest_object_is_inside_edge_rectangle:
			i = 0
			i_time = time.time()
			wait_time = 0
		
			object_center = (int(closest_object[0]+closest_object[2]/2), int(closest_object[1]+closest_object[3]-1))

			# any object is an obstacle
			print("obstacle")
			# obstacle is on the left	
			if object_center[0] < middle_obstacles(object_center[1])[0]:
				robot.move_forever(90)
#				robot.turn_forever(-45)
#				print("Turning right, {}".format(closest_object))
			# obstacle is on the right
			elif object_center[0] > middle_obstacles(object_center[1])[1]:
				robot.move_forever(90)
#				robot.turn_forever(45)
#				print("Turning left, {}".format(closest_object))
			# obstacle is straight
			elif object_center[0] > (width/2):
				np.random.choice([random.choice(actions), lambda: robot.turn_forever(45)], p=[0.0, 1.0])()
			else:
				np.random.choice([random.choice(actions), lambda: robot.turn_forever(-45)], p=[0.0, 1.0])()
		
		# marker detected, no obstacles on the way		
		else:
			i = 0
			i_time = time.time()
			wait_time = 0
			#for marker in markers:
			d1 = np.asarray(corners[0][0][1][1]) - np.asarray(corners[0][0][2][1])
			d2 = np.asarray(corners[0][0][0][1]) - np.asarray(corners[0][0][3][1])
			left_d = np.sqrt(np.sum(d1**2))
			right_d = np.sqrt(np.sum(d2**2))
			center = np.mean(corners[0][0], axis=0)
			print ("left_height: %s" % str(left_d))
			print ("right_height: %s" % str(right_d))
			# box is on the left	
			if center[0] < middle[0]:
				robot.turn_forever(45)
				print("Turning left, {}".format(center))
			# box is on the right
			elif center[0] > middle[1]:
				robot.turn_forever(-45)
				print("Turning right, {}".format(center))
			# box is straight
			elif (left_d / right_d) > 1.3:
				robot.turn(-90, 70, wait=True)
				robot.move(20, 90, wait=True)
				robot.turn(110, 70, wait=True)
			elif (right_d / left_d) > 1.3:
				robot.turn(90, 70, wait=True)
				robot.move(20, 90, wait=True)
				robot.turn(-110, 70, wait=True)	
			# box is at the bottom of the frame
			elif (left_d > 150) and (right_d > 150):
				robot.freeze()
				robot.drop_manouver_box()
				break
			else:
				robot.move_forever(90)
				print("Moving forward, {}".format(center))

# When everything done, release the capture
if __name__ == "__main__":
	box_mode()
	upper_camera.release()
	cv2.destroyAllWindows()
	








