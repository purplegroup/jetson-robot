import cv2
import numpy as np


bgr_colors = {"not_red_brick" : (255, 0, 0),
	      	  "red_brick": (0, 0, 255),
	      	  "red_thresh": (60, 100, 190),
			  "obstacle": (0, 0, 0)}

class_colors = {"obstacle": 0, "brick": 127, "background": 255}


def in_range(mean_hsv):
	mean_hsv = np.asarray(mean_hsv)
	red_hsv_lower = np.array([[0, 150, 20], [160, 150, 20]])
	red_hsv_upper = np.array([[30, 200, 255], [179, 200, 255]])
	if np.all(mean_hsv >= red_hsv_lower[0]) and np.all(mean_hsv <= red_hsv_upper[0]):
		return True
	elif np.all(mean_hsv >= red_hsv_lower[1]) and np.all(mean_hsv <= red_hsv_upper[1]):
		return True
	else:
		return False


def detect_bricks(segmented_frame, rgb_frame):
	height, width = rgb_frame.shape[:2]
	brick_frame = np.zeros((height, width, 1), np.uint8)
	brick_frame[np.where(segmented_frame == class_colors["brick"])] = 255
	obstacle_frame = np.zeros((height, width, 1), np.uint8)
	obstacle_frame[np.where(segmented_frame == class_colors["obstacle"])] = 255
	obstacle_frame[:obstacle_frame.shape[1]//6,:,:] = 0
	
	_, contours, hierarchy = cv2.findContours(brick_frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	# mask1 has rgb values of the bricks and black anywhere else
	mask1 = np.zeros((height, width, 3), np.uint8)
	mask1[brick_frame.squeeze() == 255, :] = rgb_frame[brick_frame.squeeze() == 255, :]
	circles = []
	for cnt in contours:
		(x, y), radius = cv2.minEnclosingCircle(cnt)
		center = (int(x), int(y))
		radius = int(radius)
		
		# get avg rgb value of each brick
		mask2 = np.zeros((height, width), np.uint8)
		cv2.circle(mask2, center, radius, 255, -1)
		hsv_mask1 = cv2.cvtColor(mask1, cv2.COLOR_BGR2HSV)
		pixels = hsv_mask1[np.all((mask2 == 255, np.sum(mask1, axis=-1) > 0), axis=0), :]
		# normalize
		min_v = np.min(pixels[..., -1])
		max_v = np.max(pixels[..., -1])
		pixels[..., -1] = (pixels[..., -1] - min_v) * (255 / (max_v - min_v))
		mean = np.mean(pixels, axis=0)
		print (mean)
		if in_range(mean):
			color = bgr_colors["red_brick"]
		else:
			color = bgr_colors["not_red_brick"]

		cv2.circle(rgb_frame, center, radius, color, 2)
		rgb_frame[center[1], center[0]] = color
		circles += [(center[0], center[1], radius)]

	_, contours, hierarchy = cv2.findContours(obstacle_frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	rectangles = []
	for cnt in contours:
		(x,y,w,h) = cv2.boundingRect(cnt)
		if w > (width * 2/3):
			rectangles += [(x,y,w,h)]
			cv2.rectangle(rgb_frame, (x,y), (x+w,y+h), bgr_colors["obstacle"], 2)
	return rgb_frame, circles, rectangles
	







