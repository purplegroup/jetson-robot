from robot import *
import curses
import time
import pickle

import cv2
from threading import Thread, Lock

CAM_ID = 1

camera=cv2.VideoCapture(CAM_ID)

lock = Lock()

def grab_frames():
    global frame
    while not lock.locked():
        time.sleep(0.01)
        check, frame = camera.read()
        cv2.imshow("frame", frame)
        cv2.waitKey(10)
    print("Finished grabbing")
        
t = Thread(target=grab_frames)
t.start()

screen = curses.initscr()
curses.noecho()
curses.curs_set(0)
#        curses.cbreak()
screen.keypad(1)
event = 32

read_lock = Lock()

def read_keyboard():
    global event
    while not read_lock.locked():
        time.sleep(0.01)
        screen.clear()
        screen.addstr("Control with the arrows. Halt with space. Quit with q\n")
        curses.flushinp()
        event = screen.getch()

    print("Finished reading")

t_read = Thread(target=read_keyboard)
t_read.start()

time.sleep(5)

translation_dict = {curses.KEY_LEFT: "left",
                    curses.KEY_RIGHT: "right",
                    curses.KEY_UP: "forward", 
                    curses.KEY_DOWN: "backward",
                    ord('g'): "pick",
                    ord('d'): "drop"
                   }

i = 8
while i < 100:
    while True:
        if chr(event) == 'e':
            break

    save = True
    episode = []
    while True:
        time.sleep(0.3)

        event_copy = event
        if event_copy == curses.KEY_LEFT:
            print("Turn clockwise!")
            r.turn_forever(90)
        elif event_copy == curses.KEY_RIGHT:
            print("Turn counterclockwise!")
            r.turn_forever(-90)
        elif event_copy == curses.KEY_UP:
            print("Go forward!!")
            r.move_forever(180)
        elif event_copy == curses.KEY_DOWN:
            print("Beep beep beep!! Go backwards.")
            r.move_forever(-180)
        elif chr(event_copy) == ' ': # spacebar
            print("Freeze.")
            r.freeze()
        elif chr(event_copy) == 'g':
            print("Grab")
            r.pick()
        elif chr(event_copy) == 'd':
            print("Drop")
            r.drop()
        elif chr(event_copy) == 'q': # q
            print("quit")
            r.freeze()
            break
        elif chr(event_copy) == 'r':
            print("reset episode")
            r.freeze()
            save = False
            break
        else:
            print(event_copy)

        if event_copy in (curses.KEY_LEFT,
                          curses.KEY_RIGHT,
                          curses.KEY_UP, 
                          curses.KEY_DOWN,
                          ord('g'), ord('d')
                         ):
            episode.append([frame, translation_dict[event_copy]])

    if save:
        print("Saving, length {}".format(len(episode)))
        pickle.dump(episode, open("./episodes/episode_%s.pkl" % str(i), "wb"))
        i += 1

curses.endwin()
lock.acquire()
read_lock.acquire()

#    time.sleep(0.05)
