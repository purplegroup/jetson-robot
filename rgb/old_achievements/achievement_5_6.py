import time
import numpy as np
import random
import cv2
from cameras import lower_camera
import robot
from detect_bricks2 import detect_bricks

from segmentation import get_frame, cleanup
from detect_box import box_mode


robot = robot.r

actions = [lambda: robot.turn(45, 90, wait=False), lambda: robot.turn(-45, 90, wait=False)]
actions2 = [lambda: robot.turn(90, 90, wait=True), lambda: robot.turn(-90, 90, wait=True)]
exploration_actions = [lambda: robot.move(20, 90, return_time=True),
						lambda: robot.turn(-180, 45, return_time=True),
						lambda: robot.turn(360, 45, return_time=True),
						lambda: robot.turn(-180, 45, return_time=True)]

blue = np.asarray((255,0,0))

def middle(vertical_position):
	center = int(width/2)
	bottom_deviation = int(width/6)
	top_deviation = int(width/3)
	deviation = lambda y: int(top_deviation + (bottom_deviation - top_deviation) * (y / height))
	return center - deviation(vertical_position), center + deviation(vertical_position)		

def middle_obstacles(vertical_position):
	center = int(width/2)
	bottom_deviation = int(width*0.66)
	top_deviation = int(width/6)
	deviation = lambda y: int(top_deviation + (bottom_deviation - top_deviation) * (y / height))
	return center - deviation(vertical_position), center + deviation(vertical_position)		

i = 0
i_time = time.time()
wait_time = 0
while True:
	rgb_frame, segmented_frame = get_frame(lower_camera)
	
	t0 = time.time()
	rgb_frame, object_rectangles, edge_rectangles = detect_bricks(segmented_frame, rgb_frame)
	print("Detection time {}".format(time.time() - t0))
	height, width = rgb_frame.shape[:2]
#	middle = (int(width/3), int(width*2/3))  # middle third of the frame
	cv2.line(rgb_frame, (middle(0)[0], 0), (middle(height -1)[0], height-1), (0, 255, 0))
	cv2.line(rgb_frame, (middle(0)[1], 0), (middle(height -1)[1], height-1), (0, 255, 0))
	cv2.line(rgb_frame, (middle_obstacles(0)[0], 0), (middle_obstacles(height -1)[0], height-1), (255, 255, 0))
	cv2.line(rgb_frame, (middle_obstacles(0)[1], 0), (middle_obstacles(height -1)[1], height-1), (255, 255, 0))
	cv2.imshow("rgb_frame", rgb_frame)
	cv2.waitKey(10)
	
	# find the closest object
	max_height = 0
	closest_object = (0, 0, 0, 0)
	for rectangle in object_rectangles:
		if (rectangle[1] + rectangle[3]) >= max_height:
			max_height = rectangle[1] + rectangle[3]
			closest_object = rectangle

	if edge_rectangles:
		edge_rectangles = np.asarray(edge_rectangles)
		brick_in_frame = np.any([True for rectangle in object_rectangles if np.all(rgb_frame[int(rectangle[1]+rectangle[3] -1), int(rectangle[0]+rectangle[2]/2)] == blue)])
		if not brick_in_frame:
			print("CLOSE EDGE!!! {}".format(edge_rectangles))
			random.choice(actions2)()
			continue

	# no objects in frame
	if not object_rectangles:
		if (time.time() - i_time) > wait_time:
			wait_time = exploration_actions[i % 4]()
			i = i + 1
			i_time = time.time()
	else:
		i = 0
		i_time = time.time()
		wait_time = 0
	
		object_center = (int(closest_object[0]+closest_object[2]/2), int(closest_object[1]+closest_object[3]-1))

		# closest object is brick
		if np.all(rgb_frame[object_center[1], object_center[0]] == blue):
			print("BLUE")
			# brick is on the left	
			if object_center[0] < middle(object_center[1])[0]:
				robot.turn_forever(45)
				print("Turning left, {}".format(closest_object))
			# brick is on the right
			elif object_center[0] > middle(object_center[1])[1]:
				robot.turn_forever(-45)
				print("Turning right, {}".format(closest_object))
			# brick is at the bottom of the frame
			elif (closest_object[1] + closest_object[3]) > (height - 10):
				robot.freeze()
				robot.pick_manouver()
				box_mode()
			# brick is straight
			else:
				robot.move_forever(90)
				print("Moving forward, {}".format(closest_object))

		# closest brick is obstacle
		else:
			print("RED")
			# obstacle is on the left	
			if object_center[0] < middle_obstacles(object_center[1])[0]:
				robot.move_forever(90)
#				robot.turn_forever(-45)
#				print("Turning right, {}".format(closest_object))
			# obstacle is on the right
			elif object_center[0] > middle_obstacles(object_center[1])[1]:
				robot.move_forever(90)
#				robot.turn_forever(45)
#				print("Turning left, {}".format(closest_object))
			# obstacle is straight
			elif object_center[0] > (width/2):
				np.random.choice([random.choice(actions), lambda: robot.turn_forever(20)], p=[0.0, 1.0])()
			else:
				np.random.choice([random.choice(actions), lambda: robot.turn_forever(-20)], p=[0.0, 1.0])()










											


			


	
	


	

	
	



