import numpy as np
from cameras import lower_camera
from segmentation import get_frame
from embeddings import get_lego_crop, model

NUM_OF_IMAGES = 2
	

def compute_latent_vecs():
	"""
	Compute the latent mu, std and logvar vectors of a brick.
	Take NUM_OF_IMAGES images of different orientations, and compute
	their corresponding latent vectors.
	"""
	latent_vecs = []
	
	for i in range(NUM_OF_IMAGES):
		print("image %s" % str(i+1))
		input("press ENTER to capture image")
		rgb_frame, segmented_frame = get_frame(lower_camera)
		
		# get cropped image of size 64x64 pixels
		lego_crop = get_lego_crop(rgb_frame, segmented_frame)
		
		latent_mu, latent_std, latent_logvar = model.get_latent_vectors(lego_crop)
		latent_vecs.append((latent_mu, latent_std, latent_logvar))
	return latent_vecs
		

def assign(num_of_boxes):
	"""
	Assign the clusters' latent vectors to the boxes' id markers.
	1st brick is assigned to id=0, 2nd brick to id=1 and so on...
	"""
	
	# get the latent vectors of each brick
	clusters_latent_vecs = {}
	for i in range(num_of_boxes):
		print("Taking %s images for brick %s" % (str(NUM_OF_IMAGES), str(i+1)))
		latent_vecs = compute_latent_vecs()
		clusters_latent_vecs[i] = latent_vecs
	
	return clusters_latent_vecs

