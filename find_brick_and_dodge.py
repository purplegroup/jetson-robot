import time
import random
import cv2
import numpy as np

from robot import r as robot
from cameras import lower_camera
from bound_objects import bound_objects, bgr_colors
from segmentation import get_frame, cleanup
from find_box import navigate_to_box
from assign_brick_to_box import find_cluster


middle_actions = [lambda: robot.turn(45, 90, wait=False), 
				  lambda: robot.turn(-45, 90, wait=False)]
edge_actions = [lambda: robot.turn(90, 90, wait=True), 
				lambda: robot.turn(-90, 90, wait=True)]
exploration_actions = [lambda: robot.turn(360, 45, return_time=True),
						lambda: robot.move(12, 90, return_time=True)]


brick_rectangles_color = np.asarray(bgr_colors["brick"])


def middle_bricks(vertical_position):
	"""
	Determine the horizontal position of the bricks middle lines for 
	the given vertical_position. 
	"""
	center = int(width/2)
	bottom_deviation = int(width/6)
	top_deviation = int(width/3)
	deviation = lambda y: int(top_deviation + (bottom_deviation - top_deviation) * (y / height))
	return center - deviation(vertical_position), center + deviation(vertical_position)		


def middle_obstacles(vertical_position):
	"""
	Determine the horizontal position of the obstacles middle lines for 
	the given vertical_position. 
	"""
	center = int(width/2)
	bottom_deviation = int(width*0.66)
	top_deviation = int(width/6)
	deviation = lambda y: int(top_deviation + (bottom_deviation - top_deviation) * (y / height))
	return center - deviation(vertical_position), center + deviation(vertical_position)		


def navigate_to_brick(clusters_latent_vecs):
	"""
	Search for the nearest brick and dodge obstacles on the way to it.
	"""
	global width
	global height
	i = 0
	i_time = time.time()
	wait_time = 0
	while True:
		rgb_frame, segmented_frame = get_frame(lower_camera)
		t0 = time.time()
		
		# get frame with contours
		rgb_frame, object_rectangles, edge_in_frame, middle_bottom_y = bound_objects(segmented_frame, rgb_frame)
		print("Detection time {}".format(time.time() - t0))
		height, width = rgb_frame.shape[:2]
		
		# draw bricks middle lines
		cv2.line(rgb_frame, (middle_bricks(0)[0], 0), (middle_bricks(height-1)[0], height-1), (0, 255, 0))
		cv2.line(rgb_frame, (middle_bricks(0)[1], 0), (middle_bricks(height-1)[1], height-1), (0, 255, 0))
		# draw obstacles middle lines
		cv2.line(rgb_frame, (middle_obstacles(0)[0], 0), (middle_obstacles(height-1)[0], height-1), (255, 255, 0))
		cv2.line(rgb_frame, (middle_obstacles(0)[1], 0), (middle_obstacles(height-1)[1], height-1), (255, 255, 0))
		
		cv2.imshow("lower rgb frame", rgb_frame)
		cv2.waitKey(10)
		
		# find the closest object
		max_height = 0
		closest_object = (0, 0, 0, 0)
		for rectangle in object_rectangles:
			if (rectangle[1] + rectangle[3]) >= max_height:
				max_height = rectangle[1] + rectangle[3]
				closest_object = rectangle
				
		object_center = (int(closest_object[0]+closest_object[2]/2), int(closest_object[1]+closest_object[3]-1))
				
		# edges in frame
		if edge_in_frame:
			brick_in_frame = np.any([True for rectangle in object_rectangles if np.all(rgb_frame[int(rectangle[1]+rectangle[3] -1), int(rectangle[0]+rectangle[2]/2)] == brick_rectangles_color)])
			if ((not brick_in_frame) and (middle_bottom_y > height/3)) or (brick_in_frame and middle_bottom_y > object_center[1]):
				print("CLOSE EDGE!!!")
				#random.choice(edge_actions)()
				robot.turn_forever(-45)
				continue

		# no objects in frame
		if not object_rectangles:
			print("EXPLORING...")
			if (time.time() - i_time) > wait_time:
				wait_time = exploration_actions[i % len(exploration_actions)]()
				i = i + 1
				i_time = time.time()
				
		else:
			i = 0
			i_time = time.time()
			wait_time = 0

			# closest object is brick
			if np.all(rgb_frame[object_center[1], object_center[0]] == brick_rectangles_color):
				print("brick")
				# brick is on the left	
				if object_center[0] < middle_bricks(object_center[1])[0]:
					robot.turn_forever(45)
					print("TURNING LEFT, {}".format(closest_object))
				# brick is on the right
				elif object_center[0] > middle_bricks(object_center[1])[1]:
					robot.turn_forever(-45)
					print("TURNING RIGHT, {}".format(closest_object))
				# brick is near
				elif (closest_object[1] + closest_object[3]) > (height - 30):
					print("PICK, {}".format(closest_object))
					robot.freeze()
					# decide which box to go to according to the brick cluster 
					marker_id = find_cluster(clusters_latent_vecs, rgb_frame, segmented_frame, closest_object)
					robot.pick_manouver()
					navigate_to_box(marker_id, bricks_only=False)
					#break
				# brick is straight
				else:
					robot.move_forever(90)
					print("MOVING FORWARD, {}".format(closest_object))

			# closest object is obstacle
			else:
				print("obstacle")
				# obstacle is on the left	
				if object_center[0] < middle_obstacles(object_center[1])[0]:
					robot.move_forever(90)
					print("MOVING FORWARD, {}".format(closest_object))
				# obstacle is on the right
				elif object_center[0] > middle_obstacles(object_center[1])[1]:
					robot.move_forever(90)
					print("MOVING FORWARD, {}".format(closest_object))
				# obstacle is in the middle, to the right 
				elif object_center[0] > (width/2):
					np.random.choice([random.choice(middle_actions), lambda: robot.turn_forever(20)], p=[0.0, 1.0])()
					print("TURNING LEFT, {}".format(closest_object))
				# obstacle is in the middle, to the left
				else:
					np.random.choice([random.choice(middle_actions), lambda: robot.turn_forever(-20)], p=[0.0, 1.0])()
					print("TURNING RIGHT, {}".format(closest_object))


if __name__ == "__main__":
	navigate_to_brick()
	# release the capture
	lower_camera.release()
	cv2.destroyAllWindows()

