import cv2
import time
import random
import numpy as np
import cv2.aruco as aruco

from threading import Thread, Lock
from robot import r as robot
from segmentation import get_frame, im_w, im_h
from bound_objects import bound_objects
from bound_bricks import bound_bricks
from cameras import upper_camera


middle_actions = [lambda: robot.turn(45, 20, wait=False), 
				  lambda: robot.turn(-45, 20, wait=False)]
edge_actions = [lambda: robot.turn(45, 45, wait=True), 
				lambda: robot.turn(-45, 45, wait=True)]
exploration_actions = [lambda: robot.move(12, 90, return_time=True),
					   lambda: robot.turn(360, 45, return_time=True)]
					   
					   
def middle_obstacles(vertical_position):
	"""
	Determine the horizontal position of the obstacles middle lines for 
	the given vertical_position. 
	"""
	center = int(im_w/2)
	bottom_deviation = int(im_w/2.5)
	top_deviation = int(im_w/6)
	deviation = lambda y: int(top_deviation + (bottom_deviation - top_deviation) * (y / im_h))
	return center - deviation(vertical_position), center + deviation(vertical_position)


frame_captured = False
while not frame_captured:
	frame_captured, full_frame = upper_camera.retrieve()

height_full_frame, width_full_frame = full_frame.shape[:2]
middle = (int(width_full_frame/3), int(2*width_full_frame/3))


def navigate_to_box(marker_id, bricks_only=False):
	"""
	Navigate to box with marker_id while dodging everything on the way
	to it.
	@param marker_id: the id of the marker.
	@param bricks_only: True if the frame with contours bounds only 
	bricks (when assuming there are no obstacles in the workspace), 
	False otherwise.
	"""
	height, width = im_h, im_w

	i = 0
	i_time = time.time()
	wait_time = 0
	while True:
		frame_captured, full_frame = upper_camera.retrieve()
		rgb_frame, segmented_frame = get_frame(upper_camera, apply_segm_mask=False)
		assert frame_captured
		
		# get frame with bricks and edges contours
		if bricks_only:
			rgb_frame, object_rectangles, edge_in_frame, middle_bottom_y = bound_bricks(segmented_frame, rgb_frame, is_lower_camera=False)
		# get frame with bricks, obstacles and edges contours
		else:
			rgb_frame, object_rectangles, edge_in_frame, middle_bottom_y = bound_objects(segmented_frame, rgb_frame, is_lower_camera=False)
		
		# find the closest object
		max_height = 0
		closest_object = (0, 0, 0, 0)
		for rectangle in object_rectangles:
			if (rectangle[1] + rectangle[3]) >= max_height:
				max_height = rectangle[1] + rectangle[3]
				closest_object = rectangle

		# init marker parameters
		gray = cv2.cvtColor(full_frame, cv2.COLOR_BGR2GRAY)
		aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
		parameters =  aruco.DetectorParameters_create()
		
		# detect and draw markers	 
		corners, marker_ids, rejectedImgPoints = aruco.detectMarkers(full_frame, aruco_dict, parameters=parameters)
		gray = aruco.drawDetectedMarkers(full_frame, corners, marker_ids, [0,255,0])
		
		# rearrange data structure
		try:
			if marker_ids.shape[0] == 1:
				marker_ids = marker_ids[0].tolist()
			else:
				marker_ids = np.squeeze(marker_ids).tolist()
		except:
			marker_ids = []
		
		# draw obstacles middle lines	
		cv2.line(rgb_frame, (middle_obstacles(0)[0], 0), (middle_obstacles(height-1)[0], height-1), (255, 255, 0))
		cv2.line(rgb_frame, (middle_obstacles(0)[1], 0), (middle_obstacles(height-1)[1], height-1), (255, 255, 0))
		# draw markers middle lines
		cv2.line(full_frame, (middle[0], 0), (middle[0], height_full_frame-1), (0, 255, 0))
		cv2.line(full_frame, (middle[1], 0), (middle[1], height_full_frame-1), (0, 255, 0))
		
		cv2.imshow("upper rgb frame", rgb_frame)
		cv2.imshow('full frame', full_frame)
		cv2.waitKey(10)
		
		# edges in frame
		if edge_in_frame:
			if (not marker_id in marker_ids) and (middle_bottom_y > 3*height/4):
				print("CLOSE EDGE!!!")
				#random.choice(edge_actions)()
				robot.turn_forever(-45)
				continue
		
		close_object = False
		object_center = None		
		if object_rectangles:
			object_center = (int(closest_object[0]+closest_object[2]/2), int(closest_object[1]+closest_object[3]-1))
			if object_center[1] > 3*height/4:
				close_object = True
			
		# no target marker or close objects in frame
		if (not marker_id in marker_ids) and (not close_object):
			print("EXPLORING...")
			if (time.time() - i_time) > wait_time:
				wait_time = exploration_actions[i % len(exploration_actions)]()
				i = i + 1
				i_time = time.time()
				
		# close objects in frame (any object is an obstacle)
		# objects must be before an edge!!! 
		elif close_object and ((not edge_in_frame) or (object_center[1] > middle_bottom_y)):
			i = 0
			i_time = time.time()
			wait_time = 0
		
			print("OBSTACLE")
			# object is on the left	
			if object_center[0] < middle_obstacles(object_center[1])[0]:
				robot.move_forever(90)
				#robot.turn_forever(-45)
				print("MOVING FORWARD, {}".format(closest_object))
			# object is on the right
			elif object_center[0] > middle_obstacles(object_center[1])[1]:
				robot.move_forever(90)
				#robot.turn_forever(45)
				print("MOVING FORWARD, {}".format(closest_object))
			# object is in the middle, to the right 
			elif object_center[0] > (width/2):
				print("TURNING LEFT, {}".format(closest_object))
				np.random.choice([random.choice(middle_actions), lambda: robot.turn_forever(20)], p=[0.0, 1.0])()
			# object is in the middle, to the left 
			else:
				print("TURNING RIGHT, {}".format(closest_object))
				np.random.choice([random.choice(middle_actions), lambda: robot.turn_forever(-20)], p=[0.0, 1.0])()
		
		# target marker detected, no objects on the way		
		else:
			i = 0
			i_time = time.time()
			wait_time = 0
			
			# compute vertical edges and center of marker
			marker_index = marker_ids.index(marker_id)
			d1 = np.asarray(corners[marker_index][0][1][1]) - np.asarray(corners[marker_index][0][2][1])
			d2 = np.asarray(corners[marker_index][0][0][1]) - np.asarray(corners[marker_index][0][3][1])
			left_d = np.sqrt(np.sum(d1**2))
			right_d = np.sqrt(np.sum(d2**2))
			center = np.mean(corners[marker_index][0], axis=0)
			print ("left_height: %s" % str(left_d))
			print ("right_height: %s" % str(right_d))
			
			# box is on the left	
			if center[0] < middle[0]:
				robot.turn_forever(45)
				print("TURNING LEFT, {}".format(center))
			# box is on the right
			elif center[0] > middle[1]:
				robot.turn_forever(-45)
				print("TURNING RIGHT, {}".format(center))
			# box is straight, approaching from the left
			elif (right_d / left_d) > 1.3:
				print("APPROACHING FROM LEFT")
				robot.turn(-90, 70, wait=True)
				robot.move(20, 90, wait=True)
				robot.turn(110, 70, wait=True)
			# box is straight, approaching from the right
			elif (left_d / right_d) > 1.3:
				print("APPROACHING FROM RIGHT")
				robot.turn(90, 70, wait=True)
				robot.move(20, 90, wait=True)
				robot.turn(-110, 70, wait=True)
			# box is near
			elif (left_d > 170) and (right_d > 170):
				print("DROP")
				robot.freeze()
				robot.drop_manouver_box()
				break
			# box is straight
			else:
				robot.move_forever(90)
				print("MOVING FORWARD, {}".format(center))


if __name__ == "__main__":
	marker_id = 0
	navigate_to_box(marker_id, bricks_only=False)
	# release the capture
	upper_camera.release()
	cv2.destroyAllWindows()
	
