from robot import *
import curses
import time

screen = curses.initscr()
curses.noecho()
curses.curs_set(0)
#        curses.cbreak()
screen.keypad(1)

while True:
    screen.clear()
    screen.addstr("Control with the arrows. Halt with space. Quit with q\n")
    curses.flushinp()
    event = screen.getch()
    
    if event == curses.KEY_LEFT:
        print("Turn clockwise!")
        r.turn_forever(90)
    elif event == curses.KEY_RIGHT:
        print("Turn counterclockwise!")
        r.turn_forever(-90)
    elif event == curses.KEY_UP:
        print("Go forward!!")
        r.move_forever(180)
    elif event == curses.KEY_DOWN:
        print("Beep beep beep!! Go backwards.")
        r.move_forever(-180)
    elif chr(event) == ' ': # spacebar
        print("Freeze.")
        r.freeze()
    elif chr(event) == 'g':
        print("Grab")
        r.pick()
    elif chr(event) == 'd':
        print("Drop")
        r.drop()
    elif chr(event) == 'q': # q
        curses.endwin()
        print("quit")
        r.freeze()
        break
    else:
        print(event)
#    time.sleep(0.05)
