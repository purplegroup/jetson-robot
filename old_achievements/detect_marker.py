from __future__ import print_function
import cv2
from ar_markers import detect_markers
import numpy as np


capture = cv2.VideoCapture(1)

if capture.isOpened():  # try to get the first frame
	frame_captured, frame = capture.read()
else:
	frame_captured = False

while frame_captured:
	markers = detect_markers(frame)
	for marker in markers:
		print(marker.contours)
		print(marker.contours[0])
		d1 = np.asarray(marker.contours[0][0]) - np.asarray(marker.contours[1][0])
		d2 = np.asarray(marker.contours[2][0]) - np.asarray(marker.contours[3][0])
		print(np.sqrt(np.sum(d1**2)))
		marker.highlite_marker(frame)
	cv2.imshow('Test Frame', frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
	        break
	frame_captured, frame = capture.read()

# When everything done, release the capture
capture.release()
cv2.destroyAllWindows()
