import time
from IOInterface.jetson.sensors import OnBoardCamera
import cv2
import numpy as np
import argparse


colors_on_board = {"red" : [(0, 190, 80), (10, 255, 255), (160, 190, 80), (179, 255, 255)],
				   "yellow": [(11, 190, 80), (22, 255, 255)], 
				   "orange": [(23, 0, 0), (30, 255, 255)],
				   "purple": [(120, 190, 20), (139, 255, 255)],
				   "azure": [(90, 190, 80), (110, 255, 255)],
				   "light_purple": [(140, 100, 50), (150, 255, 255)],
				   "green": [(60, 190, 20), (80, 255, 255)],
				   "light_green": [(31, 190, 0), (40, 255, 255)],
				   "pink": [(151, 150, 80), (170, 255, 255)],
				   "light_pink": [(141, 0, 80), (165, 120, 255)] }


colors_webcam = {"red" : [(0, 190, 20), (10, 255, 255), (160, 190, 20), (179, 255, 255)],
			     "yellow": [(11, 190, 20), (22, 255, 255)],
			     "azure": [(90, 190, 0), (110, 255, 255)],
			     "green": [(60, 190, 5), (80, 255, 255)]}


def detect_brick(camera):
	if camera == 0:
		colors = colors_on_board
		cam = OnBoardCamera()
	elif camera == 1:
		colors = colors_webcam
		cam = cv2.VideoCapture(1)
	#i = 81  # image counter
	while True:
		time.sleep(0.2)
		if camera == 0:
			cam_data = cam.read()
			rgb_frame = cam_data["onBoardCamera"]
		elif camera == 1:
			return_value, rgb_frame = cam.read()
			if not return_value:
				print ("no camera detected")
				continue
		hsv_frame = cv2.cvtColor(rgb_frame, cv2.COLOR_BGR2HSV)
		threshold_frame = []
		first_iteration = True
		for key, value in colors.items():
			lower = value[0]
			upper = value[1]
			threshold_frame1 = cv2.inRange(hsv_frame, lower, upper)
			if first_iteration:
				threshold_frame = threshold_frame1
				first_iteration = False
			threshold_frame = cv2.addWeighted(threshold_frame, 1.0, threshold_frame1, 1.0, 0)
			if key == "red":
				lower = value[2]
				upper = value[3]
				threshold_frame2 = cv2.inRange(hsv_frame, lower, upper)
				threshold_frame = cv2.addWeighted(threshold_frame, 1.0, threshold_frame2, 1.0, 0)
			str_el = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
			threshold_frame = cv2.morphologyEx(threshold_frame, cv2.MORPH_OPEN, str_el)
			threshold_frame = cv2.morphologyEx(threshold_frame, cv2.MORPH_CLOSE, str_el)
			_, contours, hierarchy = cv2.findContours(threshold_frame, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
			for cnt in contours:
				(x, y), radius = cv2.minEnclosingCircle(cnt)
				center = (int(x), int(y))
				radius = int(radius)
				if radius > 20:
					cv2.circle(rgb_frame, center, radius, (0, 255, 0), 2)
		#kernel = np.ones((30,30),np.uint8)
		#dilation = cv2.dilate(threshold_frame, kernel, iterations = 1)
		#cv2.imwrite('dataset/%s_original.png' % str(i), rgb_frame)
		#rgb_frame[np.where(threshold_frame == 255)] = np.array([[[255,0,0]]])
		cv2.imshow("rgb_frame", rgb_frame)
		cv2.imshow("threshold_frame", threshold_frame)
		#cv2.imwrite('dataset/%s_lego.png' % str(i), rgb_frame)
		cv2.waitKey(10)
		#i = i + 1


parser = argparse.ArgumentParser()
parser.add_argument('--camera', type=int)
args = parser.parse_args()
cam = args.camera
detect_brick(cam)







