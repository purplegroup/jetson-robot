import numpy as np
import cv2
import time

from threading import Thread, Lock

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((6*9,3), np.float32)
objp[:,:2] = np.mgrid[0:9,0:6].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

capture = cv2.VideoCapture(2)

lock = Lock()

def grab_frames():
    while not lock.locked():
        capture.grab()
        time.sleep(0.01)
    print("Finished grabbing")
        
t = Thread(target=grab_frames)
t.start()

time.sleep(2)

if capture.isOpened():  # try to get the first frame
	frame_captured, frame = capture.retrieve()
else:
	frame_captured = False

while frame_captured:
	frame = cv2.resize(frame, (320, 240))

	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

	# Find the chess board corners
	ret, corners = cv2.findChessboardCorners(gray, (9,6),None)

	# If found, add object points, image points (after refining them)
	if ret == True:
		objpoints.append(objp)

		corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
		imgpoints.append(corners2)

		# Draw and display the corners
		img = cv2.drawChessboardCorners(frame, (9,6), corners2,ret)
	cv2.imshow('img', frame)
	cv2.waitKey(10)

	frame_captured, frame = capture.retrieve()

cv2.destroyAllWindows()


	



