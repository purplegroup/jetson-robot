from IOInterface.jetson.broker import Broker
import IOInterface.jetson.sensors as sensors
import IOInterface.jetson.actuators as actuators
import ev3dev.ev3 as ev3
import time
import IOInterface.jetson.config as config
import time
from IOInterface.jetson.sensors import IMU, OnBoardCamera
import tensorflow as tf
from tensorflow import placeholder
from tensorflow.contrib.layers import fully_connected as fc
import cv2
import copy
import pdb


print("Test Cam on Jetson")
cam = OnBoardCamera()
#for _ in range(10):
while True:
    time.sleep(0.2)
    cam_data = cam.read()
    #resized = cv2.resize(cam_data["onBoardCamera"],(128,72))
    cv2.imshow("dasd", cam_data["onBoardCamera"])
    cv2.waitKey(10)
    print("cam images shape: {}".format(cam_data["onBoardCamera"].shape))


print("Test IMU on Jetson")
imu = IMU()
for _ in range(10):
    time.sleep(0.2)
    imu_data = imu.read()
    print("imu data: {}".format(imu_data))
    pdb.set_trace()

time.sleep(1)
print("Sensors work? Let's check if tensorflow works...")

# Build a 1-layer useless NN
inp_img = cam_data["onBoardCamera"]
inp_img = cv2.resize(inp_img, (32, 32))
inp_shp = inp_img.shape
x = placeholder(tf.float32, shape=inp_shp)
x_reshaped = tf.reshape(x, [1, -1])
h = fc(x_reshaped, 64)
y = fc(h, 1)
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    for _ in range(10):
        inp_img = cam.read()["onBoardCamera"]
        inp_img = cv2.resize(inp_img, (32, 32))
        pred = sess.run(y, feed_dict={x:inp_img})
        print("Useless prediction: {}".format(pred))
        time.sleep(0.2)
    
print("good luck, have fun!")

