import cv2
import numpy as np


bgr_colors = {"brick" : (255, 0, 0),
	      	  "obstacle": (0, 0, 255)}

class_colors = {"obstacle": 0, "brick": 127, "background": 255}


def detect_bricks(segmented_frame, rgb_frame, is_lower_camera=True):
	height, width = rgb_frame.shape[:2]
	brick_frame = np.zeros((height, width, 1), np.uint8)
	brick_frame[np.where(segmented_frame == class_colors["brick"])] = 255
	obstacle_frame = np.zeros((height, width, 1), np.uint8)
	obstacle_frame[np.where(segmented_frame == class_colors["obstacle"])] = 255
	if is_lower_camera:
		# ignore upper 1/6
		obstacle_frame[:obstacle_frame.shape[1]//6,:,:] = 0
	else:
		# upper camera: ignore upper region
		obstacle_frame[:obstacle_frame.shape[1]//3,:,:] = 0

	kernel = np.ones((10,10),np.uint8)
	dilation = cv2.dilate(brick_frame, kernel, iterations = 1)
	erosion = cv2.erode(obstacle_frame, kernel, iterations = 1)

	processed_frame = np.full((height, width, 1), 255, np.uint8)
	processed_frame[dilation == 255] = 127
	if is_lower_camera:
		processed_frame[erosion == 255] = 0
	else:
		processed_frame[obstacle_frame == 255] = 0
	cv2.imshow("processed_frame", processed_frame)
	
	_, contours, hierarchy = cv2.findContours(dilation, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	rectangles = []
	for cnt in contours:
		(x,y,w,h) = cv2.boundingRect(cnt)
		rectangles += [(x,y,w,h)]
		rgb_frame[int(y+h-1),int(x+w/2)] = bgr_colors["brick"]
		cv2.rectangle(rgb_frame, (x,y), (x+w,y+h), bgr_colors["brick"], 2)

	_, contours, hierarchy = cv2.findContours(erosion, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	edge_rectangles = []
	for cnt in contours:
		(x,y,w,h) = cv2.boundingRect(cnt)
		"""
		if x<=10 and y+h>=height-10:
			continue
		elif x+w >= width-10 and y+h >= height-10:
			continue
		"""
		if w*h < 600:
			continue
		if w > (width * 2/3):
			edge_rectangles += [(x,y,w,h)]
		else:
			rectangles += [(x,y,w,h)]
		cv2.rectangle(rgb_frame, (x,y), (x+w,y+h), bgr_colors["obstacle"], 2)
		
	return rgb_frame, rectangles, edge_rectangles
	







