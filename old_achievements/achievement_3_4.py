import time
import cv2
import robot
from detect_bricks import detect_bricks
import numpy as np
import random
from segmentation import get_frame, cleanup


robot = robot.r

actions = [lambda: robot.turn(45, 90, wait=True), lambda: robot.turn(-45, 90, wait=True)]
actions2 = [lambda: robot.turn(90, 90, wait=True), lambda: robot.turn(-90, 90, wait=True)]
exploration_actions = [lambda: robot.move(20, 90, return_time=True),
						lambda: robot.turn(-180, 45, return_time=True),
						lambda: robot.turn(360, 45, return_time=True),
						lambda: robot.turn(-180, 45, return_time=True)]

red = np.asarray((0,0,255))

def middle(vertical_position):
	center = int(width/2)
	bottom_deviation = int(width/6)
	top_deviation = int(width/2)
	deviation = lambda y: int(top_deviation + (bottom_deviation - top_deviation) * (y / height))
	return center - deviation(vertical_position), center + deviation(vertical_position)		

i = 0
i_time = time.time()
wait_time = 0
while True:
	rgb_frame, segmented_frame = get_frame()
	
	t0 = time.time()
	rgb_frame, brick_circles, edge_rectangles = detect_bricks(segmented_frame, rgb_frame)
	print("Detection time {}".format(time.time() - t0))
	height, width = rgb_frame.shape[:2]
#	middle = (int(width/3), int(width*2/3))  # middle third of the frame
	cv2.line(rgb_frame, (middle(0)[0], 0), (middle(height -1)[0], height-1), (0, 255, 0))
	cv2.line(rgb_frame, (middle(0)[1], 0), (middle(height -1)[1], height-1), (0, 255, 0))
	cv2.imshow("rgb_frame", rgb_frame)
	cv2.waitKey(10)
	

	# find the closest brick
	max_height = 0
	closest_brick = (0, 0, 0)
	for circle in brick_circles:
		if circle[1] >= max_height:
			max_height = circle[1]
			closest_brick = circle
"""
	if edge_rectangles:
		edge_rectangles = np.asarray(edge_rectangles)
		red_in_frame = np.any([True for circle in brick_circles if np.all(rgb_frame[circle[1], circle[0]] == red)])
		#if (not red_in_frame) and np.any((edge_rectangles[:, 1] + edge_rectangles[:, 3]) > height/2):
		if not red_in_frame:
			print("CLOSE EDGE!!! {}".format(edge_rectangles))
			random.choice(actions2)()
			continue

	# no bricks in frame
	if not brick_circles:
		if (time.time() - i_time) > wait_time:
			wait_time = exploration_actions[i % 4]()
			i = i + 1
			i_time = time.time()
	else:
		i = 0
		i_time = time.time()
		wait_time = 0

		# closest brick is red
		if np.all(rgb_frame[closest_brick[1], closest_brick[0]] == red):
			# brick is on the left	
			if closest_brick[0] < middle(closest_brick[1])[0]:
				robot.turn_forever(45)
				print("Turning left, {}".format(closest_brick))
			# brick is on the right
			elif closest_brick[0] > middle(closest_brick[1])[1]:
				robot.turn_forever(-45)
				print("Turning right, {}".format(closest_brick))
			# brick is at the bottom of the frame
			elif (closest_brick[1] + closest_brick[2]) > height:
				robot.freeze()
				robot.pick_manouver()
				break
			# brick is straight
			else:
				robot.move_forever(90)
				print("Moving forward, {}".format(closest_brick))

		# closest brick is NOT red
		else:
			# brick is at the bottom of the frame
			if (closest_brick[1] + closest_brick[2]) > height:
				robot.out_of_the_way_manouver()
			# brick is on the left	
			elif closest_brick[0] < middle(closest_brick[1])[0]:
				if closest_brick[1] < (2/3 * height):
					robot.turn_forever(-45)
					print("Turning right, {}".format(closest_brick))
				else:
					robot.turn_forever(45)
					print("Turning left, {}".format(closest_brick))
			# brick is on the right
			elif closest_brick[0] > middle(closest_brick[1])[1]:
				if closest_brick[1] < (2/3 * height):
					robot.turn_forever(45)
					print("Turning left, {}".format(closest_brick))
				else:
					robot.turn_forever(-45)
					print("Turning right, {}".format(closest_brick))
			# brick is straight
			else:
				red_in_frame = np.any([True for circle in brick_circles if np.all(rgb_frame[circle[1], circle[0]] == red)])
				if red_in_frame:
					robot.move_forever(90)
					print("Moving forward, {}".format(closest_brick))
				else:
					random.choice(actions)()
"""
											


			


	
	


	

	
	



