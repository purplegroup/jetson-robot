import time
from IOInterface.jetson.sensors import OnBoardCamera
import cv2
import numpy as np
from threading import Thread, Lock

colors = {"red" : [(0, 190, 20), (10, 255, 255), (160, 190, 20), (179, 255, 255)],
	  "yellow": [(11, 190, 20), (22, 255, 255)],
	  "green": [(60, 190, 5), (80, 255, 255)]}

rgb_colors = {"red" : (0, 0, 255),
	      "yellow": (0, 255, 255),
	      "green": (0, 255, 0) }


cam = cv2.VideoCapture(1)
cam.set(cv2.CAP_PROP_FPS, 30)

camera_lock = Lock()

def grab_frames_forever():
	while not camera_lock.locked():
		cam.grab()

t1 = Thread(target=grab_frames_forever)
t1.start()
time.sleep(2)


def detect_brick():
	return_value, rgb_frame = cam.retrieve()
	if not return_value:
		print ("no camera detected")

	hsv_frame = cv2.cvtColor(rgb_frame, cv2.COLOR_BGR2HSV)
	threshold_frames = {"red": [], "yellow": [], "green": []}
	for key, value in colors.items():
		lower = value[0]
		upper = value[1]
		threshold_frame = cv2.inRange(hsv_frame, lower, upper)
		if key == "red":
			lower = value[2]
			upper = value[3]
			threshold_frame2 = cv2.inRange(hsv_frame, lower, upper)
			threshold_frame = cv2.addWeighted(threshold_frame, 1.0, threshold_frame2, 1.0, 0)
		str_el = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
		threshold_frame = cv2.morphologyEx(threshold_frame, cv2.MORPH_OPEN, str_el)
		threshold_frame = cv2.morphologyEx(threshold_frame, cv2.MORPH_CLOSE, str_el)
		threshold_frames[key] = threshold_frame
	
	circles = []
	for key, value in threshold_frames.items():
		_, contours, hierarchy = cv2.findContours(threshold_frames[key], cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
		for cnt in contours:
			(x, y), radius = cv2.minEnclosingCircle(cnt)
			center = (int(x), int(y))
			radius = int(radius)
			if radius > 20:
				cv2.circle(rgb_frame, center, radius, rgb_colors[key], 2)
				rgb_frame[center[1], center[0]] = rgb_colors[key]
				circles += [(center[0], center[1], radius)]
	
	return rgb_frame, circles
	







