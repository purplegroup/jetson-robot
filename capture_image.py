import time
from IOInterface.jetson.sensors import OnBoardCamera
import cv2
import argparse


def capture_image(camera):
	if camera == 0:
		cam = OnBoardCamera()
	elif camera == 1:
		cam = cv2.VideoCapture(1)
		cam2 = cv2.VideoCapture(2)
	#i = 539  # image counter
	while True:
		time.sleep(0.2)
		if camera == 0:
			cam_data = cam.read()
			rgb_frame = cam_data["onBoardCamera"]
		elif camera == 1:
			return_value, rgb_frame = cam.read()
			return_value2, rgb_frame2 = cam2.read()
			if not return_value:
				print ("no camera detected")
				return
		cv2.imshow("rgb_frame", rgb_frame)
		cv2.imshow("rgb_frame2", rgb_frame2)
		#cv2.imwrite('dataset_segmentation/%s_original.png' % str(i), rgb_frame)
		cv2.waitKey(10)
		#i = i + 1


parser = argparse.ArgumentParser()
parser.add_argument('--camera', type=int)
args = parser.parse_args()
cam = args.camera
capture_image(cam)


