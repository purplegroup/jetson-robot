'''TensorFlow implementation of http://arxiv.org/pdf/1312.6114v10.pdf'''

from __future__ import absolute_import, division, print_function

import math

import numpy as np
import tensorflow as tf
from tensorflow.contrib import layers
from tensorflow.contrib import losses
from tensorflow.contrib.framework import arg_scope
import pdb
from vae.utils import encoder, decoder
from vae.generator import Generator

## load lego data
#input_folder= "../../square/"

#import fnmatch
#import os

#files = []
#for root, dirnames, filenames in os.walk(input_folder):
#    for filename in fnmatch.filter(filenames, '*.png'):
#        files.append(os.path.join(root, filename))


class VAE(Generator):

    def __init__(self, hidden_size, batch_size=1, learning_rate=1e-3, graph=tf.get_default_graph()):
        self.input_tensor = tf.placeholder(
            tf.float32, [None, 64 * 64 * 3])

        with arg_scope([layers.conv2d, layers.conv2d_transpose]):
                       #normalizer_fn=layers.batch_norm,
                       #normalizer_params={'scale': True}):
                       #activation_fn=tf.nn.elu,
                       #normalizer_params={'scale': True}):
            with tf.variable_scope("model") as scope:
                encoded = encoder(self.input_tensor, hidden_size * 2)
                #pdb.set_trace()
                mean = encoded[:, :hidden_size]
                logvar = encoded[:, hidden_size:]
                stddev = tf.sqrt(tf.exp(encoded[:, hidden_size:]))

                epsilon = tf.random_normal([tf.shape(mean)[0], hidden_size])
                input_sample = mean + epsilon * stddev
                self.embeddings = input_sample

                output_tensor = decoder(input_sample)
                self.sampled_tensor = output_tensor
                #tmp=output_tensor
                output_tensor = tf.reshape(output_tensor,[-1,64 * 64 * 3])
            # with tf.variable_scope("model", reuse=True) as scope:
            #     self.sampled_tensor = decoder(tf.random_normal(
            #         [batch_size, hidden_size]))
                
        #pdb.set_trace()
        self.latent_vectors=[mean,stddev,logvar]
        self.vae_loss = self.__get_vae_cost(mean, stddev)
        self.rec_loss = self.__get_reconstruction_cost(
            output_tensor, self.input_tensor)

        loss = self.vae_loss + self.rec_loss
        self.train = layers.optimize_loss(loss, tf.contrib.framework.get_or_create_global_step(
        ), learning_rate=learning_rate, optimizer='Adam', update_ops=[])
        # update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        # with tf.control_dependencies(update_ops):
        #     self.train = layers.optimize_loss(loss, tf.contrib.framework.get_or_create_global_step(), learning_rate=learning_rate, optimizer='Adam', update_ops=[])

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config, graph=graph)
        self.sess.run(tf.global_variables_initializer())

#        self.pl_lego_embeddings = tf.placeholder(tf.float32, [len(files), hidden_size])
#        self.lego_embeddings = tf.Variable(self.pl_lego_embeddings, name="lego_embeddings", validate_shape=False)
#        self.lego_emb_initializer = tf.variables_initializer([self.lego_embeddings])

        self.saver = tf.train.Saver(max_to_keep=1000)
        #~ writer = tf.summary.FileWriter('logs3', self.sess.graph)
        #~ writer.close()

    def __get_vae_cost(self, mean, stddev, epsilon=1e-8):
        '''VAE loss
            See the paper

        Args:
            mean:
            stddev:
            epsilon:
        '''
        return tf.reduce_sum(0.5 * (tf.square(mean) + tf.square(stddev) -
                                    2.0 * tf.log(stddev + epsilon) - 1.0))

    def __get_reconstruction_cost(self, output_tensor, target_tensor, epsilon=1e-8):
        '''Reconstruction loss

        Cross entropy reconstruction loss

        Args:
            output_tensor: tensor produces by decoder
            target_tensor: the target tensor that we want to reconstruct
            epsilon:
        '''
        target_tensor = target_tensor * 0.5 + 0.5
        weights = tf.cast(tf.greater(target_tensor, 0), tf.float32)
        return tf.reduce_sum((-target_tensor * tf.log(output_tensor + epsilon) -
                             (1.0 - target_tensor) * tf.log(1.0 - output_tensor + epsilon)) * weights)

    def update_params(self, input_tensor):
        '''Update parameters of the network

        Args:
            input_tensor: a batch of flattened images [batch_size, 28*28]

        Returns:
            Current loss value
        '''
        return self.sess.run(self.train, {self.input_tensor: input_tensor})

    def load_checkpoint(self, file_name):
        '''
        Load saved checkpoint
        '''
        self.saver.restore(self.sess, file_name)

    def get_latent_vectors(self,images):
        """
        Images are expected between -1 and 1.
        """
        images=np.reshape(images,(-1, 64*64*3))
        images = (images.astype(np.float32) / 255 - 0.5) * 2
        
        return self.sess.run(self.latent_vectors,{self.input_tensor:images})
