import tensorflow as tf
from tensorflow.contrib import layers

def encoder(input_tensor, output_size, ngf=32):
    '''Create encoder network.

    Args:
        input_tensor: a batch of flattened images [batch_size, 28*28]

    Returns:
        A tensor that expresses the encoder network
    '''

    leaky_relu = lambda x: tf.maximum(x, 0.2 * x)
    net = tf.reshape(input_tensor, [-1, 64, 64, 3])
    net = layers.conv2d(net, ngf, 4, stride=2, padding='SAME',activation_fn=None)
    net = leaky_relu(net)
    net = layers.conv2d(net, ngf*2, 4, stride=2, padding='SAME',activation_fn=None)
    #net = layers.batch_norm(net,decay=0.9,epsilon=1e-05)
    net = leaky_relu(net)
    # net = layers.conv2d(net, 128, 4, stride=2, padding='SAME')
    net = layers.conv2d(net, ngf*4, 4, stride=2, padding='SAME',activation_fn=None)
    #net = layers.batch_norm(net,decay=0.9,epsilon=1e-05)
    net = leaky_relu(net)
    net = layers.conv2d(net, ngf*8, 4, stride=2, padding='SAME',activation_fn=None)
    #net = layers.batch_norm(net,decay=0.9,epsilon=1e-05)
    net = leaky_relu(net)
    net = layers.conv2d(net,output_size,4,stride=1,padding='VALID', activation_fn=None)
    #logvar = layers.conv2d(net,output_size/2,4,stride=1,padding='VALID')
    net = tf.reshape(net, [-1, output_size])
    #print(net)
    # net = layers.dropout(net, keep_prob=0.9)
    #net = layers.flatten(net)
    return net 

def discriminator(input_tensor):
    '''Create a network that discriminates between images from a dataset and
    generated ones.

    Args:
        input: a batch of real images [batch, height, width, channels]
    Returns:
        A tensor that represents the network
    '''

    return encoder(input_tensor, 1)


def decoder(input_tensor, ngf=32):
    '''Create decoder network.

        If input tensor is provided then decodes it, otherwise samples from
        a sampled vector.
    Args:
        input_tensor: a batch of vectors to decode

    Returns:
        A tensor that expresses the decoder network
    '''

    net = tf.expand_dims(input_tensor, 1)
    net = tf.expand_dims(net, 1)
    #net = layers.conv2d_transpose(net, 64, 4, stride=1, padding='VALID',biases_initializer=None)
    leaky_relu = lambda x: tf.maximum(x, 0.2 * x)
    
    net  = layers.conv2d_transpose(net,ngf*8,4,stride=1,padding='VALID',activation_fn=None)
    #net = layers.batch_norm(net,decay=0.9,epsilon=1e-05)
    net = leaky_relu(net)
    net = layers.conv2d_transpose(net, ngf*4, 4, stride=2, padding='SAME',activation_fn=None)
    #net = layers.batch_norm(net,decay=0.9,epsilon=1e-05)
    net = leaky_relu(net)
    net = layers.conv2d_transpose(net, ngf*2, 4, stride=2, padding='SAME',activation_fn=None)
    #net = layers.batch_norm(net,decay=0.9,epsilon=1e-05)
    net = leaky_relu(net)
    net = layers.conv2d_transpose(net, ngf, 4, stride=2, padding='SAME',activation_fn=None)
    #net = layers.batch_norm(net,decay=0.9,epsilon=1e-05)
    net = leaky_relu(net)
    net = layers.conv2d_transpose(net, 3, 4, stride=2, padding='SAME',activation_fn=tf.sigmoid)

    print(net)
 
    return net
